"""
Your task is to implement the two functions below.

Note below the import statement to reuse the code that you have already seen in this week's "preclass"
"""
from module03_stacks_and_queues.part00_preclass.stack import ArrayStack

def transfer(S,T):
    """
    tranfer all the elements from stack S to stack T such that the elements at the top of S is the first
    to be inserted onto T, and the element at the bottom of S ends up at the top of T
    (that is, T will look like S with elements in reverse order)
    Note: you can assume that T is always EMPTY
    """
    while 1:
        if S.is_empty():
            break
        else:
            T.push(S.top())
            S.pop()
    T.print_contents()

def print_reverse_order(S):
    """ This function reads a sequence of words provided through command line terminated by the "stop" word
    and prints them in reverse order.
    Each word is separated by the "enter", run the main() below for how to read user input from command line
    """
    while 1:
        if S.is_empty():
            break
        print(S.top(), end=' ')
        S.pop()
if __name__ == '__main__':


    print("Example for Transfer function")

    g = ArrayStack()
    t = ArrayStack()
    print("Enter the elements of stack, if you want to terminate write {0} :".format("stop"))
    while 1:
        x = input("-->: ")
        if x == "stop":
            break
        g.push(x)

    g.print_contents()
    transfer(g, t)


    print("Example for Reverse Function")

    s = ArrayStack()
    print("Enter the elements of stack, if you want to terminate write {0} :".format("stop"))
    while 1:
        x = input("-->: ")
        if x == "stop":
            break
        s.push(x)
    print_reverse_order(s)






