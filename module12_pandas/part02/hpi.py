# add more imports as appropriate to run your code
import pandas as pd
import json
from pprint import pprint
import matplotlib.pyplot as plt
import matplotlib


# The three files in this directory bottom_tier.json, middle_tier.xls and top_tier.csv contain monthly variations of the house price index
# in Greenville, SC (USA) from 1997 until October 2017 (for instance, the value 0.67 for October 2016 means that average house prices have increased of
# of 0.67% in October 2016 compared to September 2016.
# Top/Middle/Bottom-tier refers to the type of houses (from expensive to cheap).
# Your task is to do some analysis of the provided data using Pandas, in particular you should do the following:
def imprt(file, file1, file2):
    df1 = pd.read_csv(file, sep=',')
    with open(file1) as json_data:
         df2 = json.load(json_data)
    df3 = pd.read_excel(file2, sep=',')

    ans = []
    for x in range(0, len(df1['Value'])):
        ans.append(df2['dataset']['data'][x][1] + df1['Value'][x] + df3['Value'][x])
        ans[x] /= 3
    print(len(ans), len(df1['Date']))
    plt.bar(list(df1['Date']), ans, align='center')
    plt.xticks(list(df1['Date']))
    plt.show()



if __name__ == '__main__':

    imprt("top_tier.csv", "bottom_tier.json", "middle_tier.xls")
